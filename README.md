# TP2 de programmation Web

## Mise en route

Installation des dépendances :
```
npm install
```

Lancement du serveur :
```
node server.js
```

Le serveur est alors accessible à l’adresse http://localhost:1234/

L’API Wikipedia est accessible à l’adresse http://localhost:1234/api/

## Objectif

### Prise en main de l’API Wikipedia

* Charger la page d’accueil de Wikipedia,
* lancer une recherche,
* analyser les échanges avec l’API Wikipedia,
* transposer ces échanges sur http://localhost:1234/api.

### HTML

* Ajouter un formulaire à la page HTML,
* ajouter un champ de saisie,
* ajouter un bouton,
* faire en sorte que le bouton ne valide pas le formulaire,
* ajouter un élément « div » sous le formulaire.

### JavaScript

* Définir un évènement « click » sur le bouton,
* rechercher le terme saisi dans le champ du formulaire en utilisant l’API Wikipedia disponible sur http://localhost:1234/api,
* afficher les noms de pages correspondant à la recherche dans la balise « div ».

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
